//
//  MidiMessage.cpp
//  CommandLineTool
//
//  Created by Joseph D'Souza on 29/09/2016.
//  Copyright © 2016 Tom Mitchell. All rights reserved.
//

#include "MidiMessage.hpp"

MidiMessage::MidiMessage() //Constructor - activates on creation of object of this class.
{
    number = 60;
    velocity = 64;
    channel = 1;
}

MidiMessage::MidiMessage(int initNumber, int initVelocity, int initChannel) //Constructor (with arguments) - activates on creation of object of this class.
{
    number = initNumber;
    velocity = initVelocity;
    channel = initChannel;
}

MidiMessage::~MidiMessage() //Destructor - activates on destruction of object of this class.
{
    
}
void MidiMessage::setNoteNumber (int value) //Mutator - alters values in the private area of this class.
{
    if(value > 0 && value <=127){
        number = value;}
    
    else{
        std::cout << "Invalid note value!\n";}
}
void MidiMessage::setNoteVelocity (int value) //Mutator - alters values in the private area of this class.
{
    if(value > 0 && value <=127){
        velocity = value;}
    
    else{
        std::cout << "Invalid velocity value!\n";}
}
void MidiMessage::setNoteChannel (int value) //Mutator - alters values in the private area of this class.
{
    if(value > 0 && value <=16){
        channel = value;}
    
    else{
        std::cout << "Invalid channel value!\n";}
}

int MidiMessage::getNoteNumber() const //Accessor - accesses values in the private area of this class.
{
    return number;
}
float MidiMessage::getMidiNoteInHertz() const // Accessor - accesses values in the private area of this class.
{
    return 440 * pow(2, (number-69) / 12.0);
}
float MidiMessage::getFloatVelocity() const // Accessor - accesses values in the private area of this class.
{
    return velocity / 127.0;
}
int MidiMessage::getNoteVelocity() const //Accessor - accesses values in the private area of this class.
{
    return velocity;
}
int MidiMessage::getNoteChannel() const //Accessor - accesses values in the private area of this class.
{
    return channel;
}

