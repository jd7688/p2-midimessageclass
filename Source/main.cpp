//
//  main.cpp
//  CommandLineTool
//  Software Development for Audio
//

#include <iostream>
#include "MidiMessage.hpp"

int main (int argc, const char* argv[])
{
    MidiMessage note (21, 22, 5);
    int input;
    
    std::cout << "Note: " << note.getNoteNumber() << "\nFreq: " << note.getMidiNoteInHertz();
    std::cout << "\nVelocity: " << note.getNoteVelocity() << "\nAmp: " << note.getFloatVelocity();
    std::cout << "\nChannel: " << note.getNoteChannel() << "\n\nMidi Note: ";
    
    std::cin >> input;
    note.setNoteNumber(input);
    std::cout << "Velocity: ";
    std::cin >> input;
    note.setNoteVelocity(input);
    std::cout << "Channel: ";
    std::cin >> input;
    note.setNoteChannel(input);
    
    std::cout << "\nNote: " << note.getNoteNumber() << "\nFreq: " << note.getMidiNoteInHertz();
    std::cout << "\nVelocity: " << note.getNoteVelocity() << "\nAmplitude: " << note.getFloatVelocity();
    std::cout << "\nChannel: " << note.getNoteChannel() << std::endl;
    
    return 0;
}

