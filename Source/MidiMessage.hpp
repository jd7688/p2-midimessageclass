//
//  MidiMessage.hpp
//  CommandLineTool
//
//  Created by Joseph D'Souza on 29/09/2016.
//  Copyright © 2016 Tom Mitchell. All rights reserved.
//

#ifndef MidiMessage_hpp
#define MidiMessage_hpp

#include <stdio.h>
#include <iostream>
#include <cmath>

class MidiMessage
{
public:
    MidiMessage();
    MidiMessage(int initNumber, int initVelocity, int initChannel);
    ~MidiMessage();
    void setNoteNumber (int value);
    void setNoteVelocity (int value);
    void setNoteChannel (int value);
    int getNoteNumber() const;
    float getMidiNoteInHertz() const;
    float getFloatVelocity() const;
    int getNoteVelocity() const;
    int getNoteChannel() const;
    
private:
    int number;
    int velocity;
    int channel;
};

#endif /* MidiMessage_hpp */
